#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import annotations

from pathlib import Path
from typing import Optional, TextIO, Union

import plumbum
from loguru import logger
from pydantic import FilePath, validate_arguments

from ..utils import log_cmd, warn_missing

try:
    _TRNASCAN_CMD = plumbum.local["tRNAscan-SE"]
except plumbum.CommandNotFound:
    logger.warning(warn_missing("TRNAscan-SE cli"))


@validate_arguments(config=dict(arbitrary_types_allowed=True))
def trnascan(
    island_fna_input: FilePath,
    output_file: Optional[Union[Path, TextIO]] = None,
    cpu: int = 2,
) -> Path:
    """Run TRNAscan-SE on the nucleotidic fasta file `island_fna_input` and save the
    results in `out_file` if provided.

    Parameters
    ----------
    island_faa_input : FilePath
        aminoacid fasta file
    output_file : Path | TextIO | None, optional
        Provide the handle variable to save the results. A filename or a writable
        object (as an open file). Defaults to None.

    Returns
    -------
    str
        the content of the trnascan analysis.
    """
    if island_fna_input.stat().st_size == 0:
        logger.warning(f"Empty input file: {island_fna_input}")
        return None
    cmd = _TRNASCAN_CMD[
        "-B", "--detail", "--brief", "-q", "--thread", cpu, island_fna_input
    ]
    logger.info(log_cmd(cmd))
    result_str = cmd()
    if isinstance(output_file, Path):
        with open(output_file, "w") as f:
            f.write(result_str)
    elif output_file is not None:
        output_file.write(result_str)
    return result_str
