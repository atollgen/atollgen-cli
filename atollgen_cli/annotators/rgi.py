from __future__ import annotations

from dataclasses import dataclass
from io import StringIO
from typing import Literal
from loguru import logger

import pandas as pd

from ..cmds import rgi
from .base import Annotator, Area, Island, add_annot_from_df


@dataclass
class RGIAnnotator(Annotator):
    """Annotate an island with RGI. RGI is a tool for detecting AMR genes in
    bacterial genomes. It is a wrapper around several tools, including CARD
    (https://card.mcmaster.ca/). It will work on the faa file of the island.

    Parameters
    ----------
    area : Area
        The area to annotate. Default is "island".
    origin : str
        The origin of the annotation. Default is "rgi".

    Notes
    -----
    The RGI output is a tab-separated file with the following columns:
    - ORF_ID
    - Cut_Off
    - Pass_Bitscore
    - Best_Hit_Bitscore
    - Best_Hit_ARO
    - Best_Identities
    - ARO
    - Drug Class
    - Resistance Mechanism
    - AMR Gene Family
    - Percentage Length of Reference Sequence
    """

    area: Area = "island"
    origin: str = "rgi"
    on = "faa"

    _cols = {
        "ORF_ID": "cds_id",
        "Cut_Off": "cut_off",
        "Pass_Bitscore": "pass_bitscore",
        "Best_Hit_Bitscore": "best_hit_bitscore",
        "Best_Hit_ARO": "best_hit_aro",
        "Best_Identities": "best_identities",
        "ARO": "aro",
        "Drug Class": "drug_class",
        "Resistance Mechanism": "resistance_mechanism",
        "AMR Gene Family": "amr_gene_family",
        "Percentage Length of Reference Sequence": "percentage_length_ref_seq",
    }

    def run(self, island: Island) -> str:
        """Run RGI on the island. Output the rgi output file content as string.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        str
            The content of the rgi output file.
        """
        logger.info(
            f"Running RGI  on {island.id} (origin: {self.origin}, area {self.area})"
        )
        with self.path(island) as input_faa:
            rgi(input_faa, input_faa.parent)
            return (input_faa.parent / f"{input_faa.stem}.txt").read_text()

    def process(self, island: Island) -> pd.DataFrame:
        """Run RGI, process the rgi output file content and return a dataframe with the
        annotation.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        pd.DataFrame
            A dataframe with the annotation.
        """
        logger.info(
            f"Processing RGI on {island.id} (origin: {self.origin}, area {self.area})"
        )
        rgi_content = self.run(island)
        df = pd.read_csv(StringIO(rgi_content), sep="\t")
        df = df[self._cols.keys()].rename(columns=self._cols)
        df["hit_id"] = df.cds_id.str.split(" # ").str.get(0)
        df = df.set_index("hit_id").sort_index()
        return df

    def annotate(self, island: Island) -> Island:
        """Run RGI, process the rgi output file content and add the annotation to the
        island. Island is modified in place.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        Island
            The annotated island.
        """
        try:
            df = self.process(island)
        except RuntimeError:
            return island
        add_annot_from_df(island, df, origin=self.origin)
        return island
