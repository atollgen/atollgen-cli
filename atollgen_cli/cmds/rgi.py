#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import annotations

from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional

import plumbum
from loguru import logger
from pydantic import FilePath, validate_arguments

from ..utils import log_cmd, warn_missing

try:
    _RGI_CMD = plumbum.local["rgi"]
except plumbum.CommandNotFound:
    logger.warning(warn_missing("rgi cli"))

try:
    _SED_CMD = plumbum.local["sed"]
except plumbum.CommandNotFound:
    logger.warning(warn_missing("sed"))


def load_card_json(card_json_file: Optional[FilePath] = None) -> None:
    """Load the card json file `card_json_file` in the rgi database. If `card_json_file`
    is None, the default card json file will be downloaded and loaded.

    Parameters
    ----------
    card_json_file : FilePath, optional
        card json file, by default None

    """

    cmd = _RGI_CMD["load"]
    if card_json_file:
        cmd = cmd["--card_json", card_json_file]
    logger.info(log_cmd(cmd))
    stdout = cmd()
    if stdout:
        logger.info(stdout)
    return


@validate_arguments
def rgi(faa_input: FilePath, out_dir: Path, *, cpu=2) -> Path:
    """Run RGI on the aminoacid fasta file `island_faa_input` and save the results in
    `out_dir`.

    Parameters
    ----------
    island_faa_input : FilePath
        aminoacid fasta file
    out_dir : Path
        output folder

    Returns
    -------
    Path
        output folder

    Notes
    -----

    RGI does not support fasta files with stop codons. This function removes the stop
    codons before running RGI.

    The RGI command is:

        rgi main --clean -n <cpu> -t protein -i <faa_input> -o <out_dir>
    """
    out_dir.mkdir(exist_ok=True)
    with TemporaryDirectory() as d:
        d = Path(d)
        faa_nostop = str(d / f"{faa_input.stem}.nostop")
        (_SED_CMD["s/*//g", faa_input] > faa_nostop)()
        cmd = _RGI_CMD[
            "main",
            "--clean",
            "-n",
            cpu,
            "-t",
            "protein",
            "-i",
            faa_nostop,
            "-o",
            out_dir / faa_input.stem,
        ]
        logger.info(log_cmd(cmd))
        _, stdout, stderr = cmd.run()
        if stderr:
            raise RuntimeError(stderr)
        if stdout:
            logger.debug(stdout)
    return out_dir
