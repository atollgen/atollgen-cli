""" Sequence handling utilities. All the functions and classes needed to handle genome
sequences, and how to split them according to the islands coordinates.

Areas of the islands are:

- left_env: 2000bp upstream of the island ([start_island - 2000, start_island])
- left_ext: 5000bp upstream of the island ([start_island, start_island + 5000)
- bulk: the bulk of the island ([start_island + 5000, end_island - 5000])
- right_ext: 5000bp downstream of the island ([end_island - 5000, end_island])
- right_env: 2000bp downstream of the island ([end_island, end_island + 2000])

These coordinates are clamped to the genome boundaries.

The compounds areas are:

- envs: left_env + right_env
- exts: left_ext + right_ext
- island: bulk + exts
- all: envs + island
"""
from __future__ import annotations

import re
from collections import defaultdict
from contextlib import contextmanager
from copy import deepcopy
from dataclasses import InitVar, field
from functools import cached_property
from itertools import chain
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Dict, List, Literal, Optional, Union
from typing_extensions import Self

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from entrez_fetcher import GenomeStore
from intervaltree import Interval, IntervalTree
from pydantic import FilePath, validator
from pydantic.dataclasses import dataclass
from typing import Dict

from .cmds import prodigal

_PRODIGAL_CDS_PATTERN = re.compile(
    r"^(?P<cds_id>(?P<chr>\S+):(?P<island_start>\d+)..(?P<island_end>\d+)_\d+)\s+#\s+(?P<cds_start>\d+)\s+#\s+(?P<cds_end>\d+)\s+#\s+(?P<cds_strand>-?1)\s+#\s+(?P<cds_extra>.*)$"
)


def get_island_bounds(start: int, end: int, host_size: int) -> Dict[str, int]:
    """Get the bounds of the island.

    Parameters
    ----------
    start : int
        The start of the island.
    end : int
        The end of the island.
    host_size : int
        The size of the host genome.

    Returns
    -------
    Dict[str, int]
        The bounds of the island.
    """
    return {
        "left_env": max(start - 2000, 1),
        "start_island": start,
        "left_ext": min(start + 5000, end),
        "right_ext": max(end - 5000, start),
        "end_island": end,
        "right_env": max(min(end + 2000, host_size), end),
    }


_AREAS_FROM_TO = {
    "left_env": ("left_env", "start_island"),
    "left_ext": ("start_island", "left_ext"),
    "bulk": ("left_ext", "right_ext"),
    "right_ext": ("right_ext", "end_island"),
    "right_env": ("end_island", "right_env"),
}


def coords_to_areas(coords: Dict[str, int]) -> Dict[str, Interval]:
    """Convert coordinates to areas.

    Parameters
    ----------
    coords : Dict[str, int]
        The coordinates of the island.

    Returns
    -------
    Dict[str, Interval]
        The areas of the island.
    """
    areas = {}
    for area, bounds in _AREAS_FROM_TO.items():
        begin, end = map(coords.get, bounds)
        if begin < end:
            areas[area] = Interval(begin, end)
    return areas


def get_distance(cds_start: int, cds_end: int, bounds: dict, area: str) -> int:
    """Get the distance of a CDS to the island.

    Parameters
    ----------
    cds_start : int
        The start of the CDS.
    cds_end : int
        The end of the CDS.
    bounds : dict
        The bounds of the island.
    area : str
        The area of the island.
    """
    if "left" in area:
        return int(min(bounds["start_island"] - x for x in (cds_start, cds_end)))
    if "right" in area:
        return int(min(x - bounds["end_island"] for x in (cds_start, cds_end)))
    return None


def categorise_cds(
    cds_start: int,
    cds_end: int,
    areas: Dict[str, Interval],
) -> str:
    """Categorise a CDS according to its position in the island.

    The CDS is considered to be in the "left_env" area if it overlaps with the left
    environment by more than 10% of its size. The same applies to the "right_env" area.

    If the CDS is inside the left part of the island, it is considered to be in the
    "left_ext" area if it is in the first 5000bp of the island. Otherwise, it is
    considered to be in the "bulk" area. The same applies to the right part of the
    island.

    Parameters
    ----------
    cds_start : int
        The start of the CDS.
    cds_end : int
        The end of the CDS.
    areas : Dict[str, Interval]
        The areas of the island.

    Returns
    -------
    str
        The area of the island where the CDS is located.
    """
    size = cds_end - cds_start
    overlap_size = {
        area: interval.overlap_size(begin=cds_start, end=cds_end)
        for area, interval in areas.items()
    }
    if overlap_size.get("left_env", False) > 0.1 * size:
        return "left_env"
    if overlap_size.get("right_env", False) > 0.1 * size:
        return "right_env"
    if overlap_size["left_ext"] > 0:
        return "left_ext"
    if overlap_size["right_ext"] > 0:
        return "right_ext"
    return "bulk"


@dataclass
class Prodigal_CDS:
    """A CDS predicted by Prodigal.

    Parameters
    ----------
    areas : Dict[str, Interval]
        The areas of the island.
    cds_id : str
        The ID of the CDS.
    chr : str
        The chromosome of the CDS.
    island_start : int
        The start of the island.
    island_end : int
        The end of the island.
    cds_start : int
        The start of the CDS.
    cds_end : int
        The end of the CDS.
    cds_strand : Union[int, Literal["+", "-"]]
        The strand of the CDS.
    cds_extra : str
        The extra information of the CDS as a string.
    cds_size : Optional[int], optional
        The size of the CDS, by default None
    area : Optional[str], optional
        The area of the island where the CDS is located, by default None
    record : Optional[Any], optional
        The record of the CDS, by default None
    """

    areas: InitVar(Dict[str, Interval])
    cds_id: str
    chr: str
    island_start: int
    island_end: int
    cds_start: int
    cds_end: int
    cds_strand: Union[int, Literal["+", "-"]]
    cds_extra: str
    cds_size: Optional[int] = None
    area: Optional[str] = None
    record: Optional[Any] = field(default=None, repr=False)

    @validator("cds_strand")
    def _coerce_strand(cls, v):
        if v == 1:
            return "+"
        elif v == -1:
            return "-"
        return v

    def __post_init_post_parse__(self, areas):
        self.cds_start += self.island_start - 1
        self.cds_end += self.island_start - 1
        self.cds_size = self.cds_end - self.cds_start
        self.cds_start, self.cds_end = sorted((self.cds_start, self.cds_end))
        self.area = categorise_cds(self.cds_start, self.cds_end, areas)


def localize_cds(
    faa_file: Path, areas: List[Dict[str, Interval]]
) -> Dict[str, Prodigal_CDS]:
    """Localize the CDS in the island.

    Parameters
    ----------
    faa_file : Path
        The path to the file containing the CDS.
    areas : List[Dict[str, Interval]]
        The interval of the island areas.

    Returns
    -------
    Dict[str, Prodigal_CDS]
        The CDS localized in the island.
    """
    if not Path(faa_file).stat().st_size:
        return {}
    cds_infos = [
        Prodigal_CDS(areas, **match.groupdict(), record=record)
        for record in SeqIO.parse(faa_file, "fasta")
        if (match := _PRODIGAL_CDS_PATTERN.match(record.description))
    ]

    return cds_infos


COMPOUND_AREAS = {
    "island": (
        "left_ext",
        "bulk",
        "right_ext",
    ),
    "envs": (
        "left_env",
        "right_env",
    ),
    "exts": (
        "left_ext",
        "right_ext",
    ),
    "all": (
        "left_env",
        "left_ext",
        "bulk",
        "right_ext",
        "right_env",
    ),
}

Area = Literal[
    "left_env",
    "left_ext",
    "bulk",
    "right_ext",
    "right_env",
    "island",
    "envs",
    "exts",
    "all",
]


def _slice_record(rec: SeqIO.SeqRecord, start: int, end: int):
    rec = deepcopy(rec)
    rec.seq = rec.seq[start - 1 : end - 1]
    rec.id = f"{rec.name}:{start}..{end}"
    return rec


def _split_cds(cds_info: List[Prodigal_CDS], only_record=True):
    res = defaultdict(list)
    for cds in cds_info:
        res[cds.area].append(cds.record if only_record else cds)
    return dict(res)


@dataclass
class SeqHandler:
    """A class to handle the sequence of the island.

    Parameters
    ----------
    areas : Dict[str, Interval]
        The interval of the island areas.
    genome_filename : FilePath
        The path to the genome file.

    Attributes
    ----------
    genome_record : SeqIO.SeqRecord
        The record of the genome.
    cds_info : List[Prodigal_CDS]
        The information of each CDS in the island and its environment.
    cds_info_by_area : Dict[str, List[Prodigal_CDS]]
        The information of each CDS in the island and its environment, grouped by area.
    """

    areas: Dict[str, Interval]
    genome_filename: FilePath

    @classmethod
    def from_island(cls, island: "Island", genome_store: GenomeStore = None) -> Self:
        """Create a SeqHandler from an Island.

        Parameters
        ----------
        island : Island
            The island to create the SeqHandler from.
        genome_store : GenomeStore, optional
            The genome store to use, by default None. If None, a new GenomeStore will be
            created.

        Returns
        -------
        SeqHandler
            The SeqHandler created from the island.
        """
        if genome_store is None:
            genome_store = GenomeStore()
        genome_filename = genome_store.get_genome_filename(island.accession)
        return cls(island.areas, genome_filename)

    @cached_property
    def genome_record(self):
        return SeqIO.read(self.genome_filename, "fasta")

    @cached_property
    def cds_info(self):
        with TemporaryDirectory() as d:
            d = Path(d)
            self.save_fna_area(d / "all.fna", "all")
            prodigal(d / "all.fna", d / "all.faa")
            return localize_cds(d / "all.faa", self.areas)

    @cached_property
    def cds_info_by_area(self):
        return _split_cds(self.cds_info)

    def get_fna_area(
        self,
        area: Area = "island",
        squeeze: bool = True,
    ) -> Union[SeqRecord, List[SeqRecord]]:
        """Get the sequence of the area.

        Parameters
        ----------
        area : Area, optional
            The area to get the sequence from, by default "island".
        squeeze : bool, optional
            Whether to squeeze the result if there is only one record, by default True.

        Returns
        -------
        SeqRecord | List[SeqRecord]
            The sequence of the area. If squeeze is True and there is only one record,
            the record will be returned directly. Otherwise, a list of records will be
            returned.
        """

        def extend_area_env(area, bound):
            if area == "left_env":
                return Interval(bound[0], bound[1] + 300)
            if area == "right_env":
                return Interval(bound[0] - 300, bound[1])
            return bound

        if areas := COMPOUND_AREAS.get(area, False):
            tree = IntervalTree(
                [
                    extend_area_env(area, self.areas[area])
                    for area in areas
                    if area in self.areas
                ]
            )
            tree.merge_overlaps(strict=False)
            bounds = tree.all_intervals
        else:
            bounds = [extend_area_env(area, self.areas[area])]
        recs = [_slice_record(self.genome_record, *bound[:2]) for bound in bounds]
        if squeeze and len(recs) == 1:
            return recs[0]
        return recs

    def get_faa_area(
        self,
        area: Area = "island",
    ) -> List[Prodigal_CDS]:
        """Get the CDS of the area.

        Parameters
        ----------
        area : Area, optional
            The area to get the CDS from, by default "island".

        Returns
        -------
        List[Prodigal_CDS]
            The CDSs of the area.
        """
        cds_info_by_area = self.cds_info_by_area
        if areas := COMPOUND_AREAS.get(area, False):
            return list(chain.from_iterable(self.get_faa_area(area) for area in areas))
        else:
            return list(cds_info_by_area.get(area, []))

    @contextmanager
    def path(self, kind: Literal["fna", "faa"], area: Area = "island"):
        """Get the path to the sequence of the area, in a temporary directory. The file
        will be deleted after the context manager exits.

        Parameters
        ----------
        kind : Literal["fna", "faa"]
            The kind of the sequence.
        area : Area, optional
            The area to get the sequence from, by default "island".
        """
        with TemporaryDirectory() as d:
            filename = Path(d) / f"seq.{kind}"
            if kind == "fna":
                self.save_fna_area(filename, area)
            if kind == "faa":
                self.save_faa_area(filename, area)
            yield filename

    def save_fna_area(
        self,
        filename: Path,
        area: Area = "island",
    ):
        """Save the nucleotide sequence of the area to a file.

        Parameters
        ----------
        filename : Path
            The path to the file.
        area : Area, optional
            The area to get the sequence from, by default "island".
        """
        SeqIO.write(self.get_fna_area(area), filename, format="fasta")

    def save_faa_area(
        self,
        filename: Path,
        area: Area = "island",
    ):
        """Save the amino acid sequence of the area to a file.

        Parameters
        ----------
        filename : Path
            The path to the file.
        area : Area, optional
            The area to get the CDS from, by default "island".
        """
        SeqIO.write(self.get_faa_area(area), filename, format="fasta")
