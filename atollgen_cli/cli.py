"""Command line interface for atollgen"""

import re
import sys
from datetime import timedelta
from pathlib import Path
from typing import Optional

import joblib
import numpy as np
import pandas as pd
import typer
from loguru import logger
from simple_slurm import Slurm

from .runner import DataHolder, AtollgenRunner, USER_APPDIR, LocalGenomeStore


_ISLAND_ID_PATTERN = re.compile(
    r"(?P<accession>\S+?)(\.(?:\d+))?:(?P<start>\d+)\.\.(?P<end>\d+)"
)


app = typer.Typer()


@app.command()
def preprocess(
    verbosity: str = "INFO",
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    logger.remove()
    logger.add(sys.stderr, level=verbosity)
    data_holder = DataHolder(db_dir=db_dir)
    data_holder.load()


@app.command()
def preload(
    island_csv: typer.FileText,
    verbosity: str = "INFO",
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    logger.remove()
    logger.add(sys.stderr, level=verbosity)
    island_info = pd.read_csv(island_csv)
    if not isinstance(island_info, pd.DataFrame):
        raise ValueError("island_csv should be a csv file")
    data_holder = DataHolder(db_dir=db_dir)
    data_holder.preload_from_ncbi(island_info["accession"])


@app.command()
def run(
    island_id: str,
    save: Optional[Path] = None,
    quiet: bool = False,
    verbosity: str = "INFO",
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    logger.remove()
    logger.add(sys.stderr, level=verbosity)
    runner = AtollgenRunner(db_dir=db_dir)
    try:
        runner.check()
    except Exception as e:
        typer.echo(
            f"Annotator check failed, atollgen-cli preprocess may be needed: {e}",
            err=True,
        )
        raise
    try:
        match = _ISLAND_ID_PATTERN.match(island_id)
        if not match:
            raise ValueError(f"invalid island_id: {island_id}")
        island_info = match.groupdict()
        accession = island_info.pop("accession")
        start = int(island_info.pop("start"))
        end = int(island_info.pop("end"))
        island = runner.run(accession, start, end)
        if save:
            with open(save, "w") as f:
                f.write(island.to_json())  # type: ignore
        if not quiet:
            typer.echo(island.to_json())
        return island.to_dict()
    except Exception as e:
        typer.echo(f"error on uid {island_id}: {e}", err=True)
        raise


@app.command()
def run_local(
    fasta: Path,
    coords: str,
    taxid: int,
    save: Optional[Path] = None,
    quiet: bool = False,
    verbosity: str = "INFO",
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    logger.remove()
    logger.add(sys.stderr, level=verbosity)
    local_store = LocalGenomeStore(fasta, taxid)
    runner = AtollgenRunner(db_dir=db_dir, genome_store=local_store)
    island_id = f"{fasta.stem}:{coords}"
    try:
        runner.check()
    except Exception as e:
        typer.echo(
            f"Annotator check failed, atollgen-cli preprocess may be needed: {e}",
            err=True,
        )
        raise
    try:
        start, end = map(int, coords.split(".."))
        island = runner.run(fasta.stem, start, end, tax_id=taxid)
        if save:
            with open(save, "w") as f:
                f.write(island.to_json())  # type: ignore
        if not quiet:
            typer.echo(island.to_json())
        return island.to_dict()
    except Exception as e:
        typer.echo(f"error on uid {island_id}: {e}", err=True)
        raise


@app.command()
def many(
    island_db: typer.FileText,
    output_folder: Path,
    n_jobs: int = -1,
    overwrite: bool = False,
    verbosity: str = "INFO",
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    logger.remove()
    logger.add(sys.stderr, level=verbosity)
    runner = AtollgenRunner(db_dir=db_dir)
    try:
        runner.check()
    except Exception as e:
        typer.echo(
            f"Annotator check failed, atollgen-cli preprocess may be needed: {e}",
            err=True,
        )
        raise
    output_folder = Path(output_folder)
    output_folder.mkdir(exist_ok=True, parents=True)
    island_info = pd.read_csv(island_db)
    if not isinstance(island_info, pd.DataFrame):
        raise ValueError("island_db should be a csv file")
    runner.data_holder.preload_from_ncbi(island_info.accession)
    runner.data_holder.genome_store.read_only = True
    with joblib.parallel_backend("loky", n_jobs=n_jobs):
        runner.run_many(
            island_info[["accession", "start", "end"]].itertuples(index=False),
            output_folder,
            overwrite=overwrite,
        )


@app.command()
def batch(
    island_db: typer.FileText,
    output_folder: Path,
    batch: str,
    n_jobs: int = -1,
    overwrite: bool = False,
    verbosity: str = "INFO",
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    i, n = map(int, batch.split("/"))
    if not (1 <= i <= n):
        raise ValueError("batch should be written as i/n with 1 <= i <= n")

    logger.remove()
    logger.add(sys.stderr, level=verbosity)
    runner = AtollgenRunner(db_dir=db_dir)
    runner.data_holder.genome_store.read_only = True
    try:
        runner.check()
    except Exception as e:
        typer.echo(
            f"Annotator check failed, atollgen-cli preprocess may be needed: {e}",
            err=True,
        )
        raise
    output_folder = Path(output_folder)
    output_folder.mkdir(exist_ok=True, parents=True)
    island_info = pd.read_csv(island_db)
    if not isinstance(island_info, pd.DataFrame):
        raise ValueError("island_db should be a csv file")
    chunk = np.array_split(island_info, n)[i - 1]
    with joblib.parallel_backend("loky", n_jobs=n_jobs):
        runner.run_many(
            chunk[["accession", "start", "end"]].itertuples(index=False),
            output_folder,
            overwrite=overwrite,
        )
    return


@app.command()
def sbatch(
    island_db: Path,
    output_folder: Path,
    account: Optional[str] = None,
    log_dir: Path = Path("./log"),
    batch_size: int = 200,
    exec_time: int = 120,
    exec_mem: int = 100,
    cpu: int = 24,
    overwrite: bool = False,
    dry: bool = True,
    db_dir: Path = typer.Option(
        default=USER_APPDIR, file_okay=False, resolve_path=True
    ),
):
    island_db = Path(island_db).absolute()
    output_folder = Path(output_folder).absolute()
    log_dir = Path(log_dir).absolute()
    eta = batch_size / (cpu / 2) * exec_time
    eta_str = str(timedelta(seconds=eta * 2))
    islands_info = pd.read_csv(island_db)
    if not isinstance(islands_info, pd.DataFrame):
        raise ValueError("island_db should be a csv file")
    data_holder = DataHolder(db_dir=db_dir)
    data_holder.preload_from_ncbi(islands_info.accession)

    njobs = len(islands_info) // batch_size + 1
    slurm = Slurm(
        time=eta_str,
        cpus_per_task=cpu,
        mem_per_cpu=f"{exec_mem // 2}M",
        output=log_dir / "slurm-%j.out",
        error=log_dir / "slurm-%j.err",
        array=f"1-{njobs}",
    )
    logger.info(slurm)
    if account:
        slurm.add_arguments(account=account)
    commandline = (
        f"atollgen-cli batch --db-dir {db_dir} {island_db} {output_folder} "
        f"$SLURM_ARRAY_TASK_ID/{njobs} --n-jobs={cpu//2}"
    )
    if overwrite:
        commandline += " --overwrite"
    typer.echo(str(slurm) + "\n" + commandline)
    if dry:
        return
    job_id = slurm.sbatch(commandline)
    return job_id


if __name__ == "__main__":
    logger.enable("atollgen")
    app()
