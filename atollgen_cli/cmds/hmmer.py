#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Manipulation of hmmer command lines (hmmsearch, hmmpress, hmmconvert) and hmm files
manipulation (basic concatenation / explore /split / filtering).

TODO: use pyhmmer instead of hmmer cli (https://github.com/althonos/pyhmmer)
"""

from __future__ import annotations

import re
from pathlib import Path
from typing import List, Optional, Sequence, TextIO, Union

import plumbum
from filelock import FileLock
from loguru import logger
from pydantic import BaseModel, FilePath, validate_arguments

from ..utils import log_cmd, warn_missing

try:
    _HMMCONVERT_CMD = plumbum.local["hmmconvert"]
    _HMMPRESS_CMD = plumbum.local["hmmpress"]
    _HMMSEARCH_CMD = plumbum.local["hmmsearch"]
except plumbum.CommandNotFound:
    logger.warning(warn_missing("HMMer commandlines"))


@validate_arguments
def hmmpress(hmm_file: FilePath, force: bool = False) -> None:
    """Run hmmpress on the provided hmmfile. Avoid reruning if they are already here.
    Thread / MultiProcess safe via a file lock.

    Parameters
    ----------
    hmm_file : Path
        The HMMFile.

    force : bool, optional
        rerun the press even if the files are present. Defaults to False.

    Notes
    -----

    hmmpress is a command line from HMMer that create 4 files from a HMMFile:
    - .h3f
    - .h3i
    - .h3m
    - .h3p

    """
    press_generated_files = [
        Path(f"{hmm_file}.{ext}") for ext in ["h3f", "h3i", "h3m", "h3p"]
    ]

    with FileLock(hmm_file.parent / f".{hmm_file.stem}.press.lock", timeout=60):
        if not all(file.exists() for file in press_generated_files) or force:
            [file.unlink(missing_ok=True) for file in press_generated_files]
            cmd = _HMMPRESS_CMD[hmm_file]
            logger.info(log_cmd(cmd))
            logger.debug(cmd())


@validate_arguments
def hmmsearch(
    hmm_file: FilePath, fasta_file: FilePath, domain_out: Path, *, cpu=2, press=True
) -> Path:
    """Run hmmsearch on the provided file accross a hmmfile database.

    Parameters
    ----------
    hmm_file : FilePath
        HMMFile database
    fasta_file : FilePath
        Fasta file where to search
    domain_out : Path
        HMMer domain output file
    force : bool, optional
        set True to force the search even if domain_out exists.
        Defaults to False.
    cpu : int, optional
        number of cpu used for analysis. Defaults to 3 (2 for HMMer
        analysis and 1 as main thread).
    press : bool, optional
        whether to press the hmm_file. Defaults to True

    Returns
    -------
    Path
        The domain_out file.

    Notes
    -----
    hmmsearch is a command line from HMMer that search a fasta file accross a HMMFile
    database.

    The command line is:

        hmmsearch --domtblout <domain_out> --cpu <cpu -1> <hmm_file> <fasta_file>
    """
    domain_out = Path(domain_out)
    if press:
        hmmpress(hmm_file)
    cmd = _HMMSEARCH_CMD[
        "--domtblout", domain_out, "--cpu", cpu - 1, hmm_file, fasta_file
    ]
    logger.info(log_cmd(cmd))
    logger.debug(cmd())
    return domain_out


_PATTERN_PFAM_DESC = re.compile(
    r"NAME\s*(?P<query_id>.*)\n(?:ACC\s*(?P<pfam_full>(?P<pfam_id>PF(?P<base_id>\d*)).(?P<pfam_ver>\d*))\n)?(?:DESC\s*(?P<pfam_desc>.*))?"
)


class HMMEntry(BaseModel):
    query_id: str
    pfam_full: Optional[str]
    base_id: Optional[int]
    pfam_ver: Optional[int]
    pfam_desc: Optional[str]


@validate_arguments
def inspect_hmm_file(hmm_file: FilePath) -> List[HMMEntry]:
    """Get the list of the hmmfile entries.

    Parameters
    ----------
    hmm_file : Path
        The HMMFile.

    Returns
    -------
    List[HMMEntry]
        The HMMEntries. Convert it as a dataframe with DataFrame(map(dict, entries)).
    """
    with open(hmm_file) as f:
        return [
            HMMEntry(**match.groupdict())
            for match in _PATTERN_PFAM_DESC.finditer(f.read())
        ]


_PATTERN_PFAM = re.compile(r"ACC\s*((PF\d*).(\d*))")


@validate_arguments(config=dict(arbitrary_types_allowed=True))
def split_hmmfile(
    hmm_file: FilePath,
    pfam_list: Union[str, Sequence[str]],
    *,
    output_file: Optional[Union[Path, TextIO]] = None,
) -> str:
    """Allow to split an hmm file in order to keep only some pfam ids.

    Parameters
    ----------
    hmm_file : Path
        HMMfile.
    pfam_list : str | List[str]
        Pfam ids subset list.
    output_file : Path | TextIO, optional
        Provide the handle variable to save the results. A filename or a
        writable object (as an open file). Defaults to None.

    Returns
    -------
    str
        the content of the splitted hmmfile.
    """
    if isinstance(pfam_list, str):
        pfam_list = [pfam_list]
    with open(hmm_file) as f:
        chunks = f.read().split("//")
    relevant_chunks = []
    for chunk in chunks:
        match = _PATTERN_PFAM.search(chunk)
        if match is None:
            continue
        if match.groups()[1] in pfam_list:
            relevant_chunks.append(chunk.strip("\n"))
    result_str = "\n//\n".join(relevant_chunks)
    if result_str:
        result_str += "\n//"

    if isinstance(output_file, Path):
        output_file = open(output_file)
    if output_file is not None:
        output_file.write(result_str)

    return result_str


_NAME_PATTERN = re.compile(r"^(NAME\W+)(.+)$", flags=re.MULTILINE)


@validate_arguments(config=dict(arbitrary_types_allowed=True))
def concat_hmmfiles(
    hmm_files: List[FilePath], *, output_file: Optional[Union[Path, TextIO]] = None
) -> str:
    """Concatenate all HMM files.

    Parameters
    ----------
    hmm_files : Path
        list of HMMfiles.
    output_file : Path | TextIO, optional
        Provide the handle variable to save the results. A filename or a
        writable object (as an open file).  Defaults to None.

    Returns
    -------
    str
        the content of the concatenated hmm files.
    """

    def _process_file(hmm_file):
        hmm_file = Path(hmm_file)
        return _NAME_PATTERN.sub(
            f"\\1{hmm_file.basename().stripext()}", _HMMCONVERT_CMD(hmm_file)
        )

    cat_str = "\n".join(map(_process_file, hmm_files))

    if isinstance(output_file, Path):
        output_file = open(output_file)
    if output_file is not None:
        output_file.write(cat_str)
    return cat_str


@validate_arguments(config=dict(arbitrary_types_allowed=True))
def build_hmm(
    *,
    pfam_db: FilePath,
    pfam_list: Union[str, Sequence[str]] = None,
    hmm_files: List[FilePath] = None,
    output_file: Optional[Union[Path, TextIO]] = None,
) -> str:
    """Rebuild a custom hmm file from

    - subset of the pfam database (`pfam_db`) filtered with `pfam_list`
    - a list of custom hmm files signatures (`hmm_files`)

    Parameters
    ----------
    pfam_db : FilePath
        PFAM database as a hmmfile.
    pfam_list : str | List[str]
        Pfam ids subset list. Defaults to None
    hmm_files : List[FilePath]
        list of HMMfiles. Defaults to None
    output_file : Path | TextIO, optional
        Provide the handle variable to save the results. A filename or a writable
        object (as an open file). Defaults to None.

    Returns
    -------
    str
        the content of the new hmm file.
    """
    hmm_strs = []
    if hmm_files is not None:
        hmm_strs.append(concat_hmmfiles(hmm_files))
    if pfam_list is not None:
        hmm_strs.append(split_hmmfile(pfam_db, pfam_list))
    cat_str = "\n".join(hmm_strs)
    if isinstance(output_file, Path):
        output_file = open(output_file)
    if output_file is not None:
        output_file.write(cat_str)
    return cat_str
