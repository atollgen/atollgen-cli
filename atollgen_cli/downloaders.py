from __future__ import annotations

import contextlib
import hashlib
import os
from pathlib import Path
from typing import Optional, Union

import requests
from tqdm import tqdm


@contextlib.contextmanager
def working_directory(path):
    """ Context manager for changing the current working directory

    Parameters
    ----------
    path : str | Path
        The path to change the working directory to
    """
    prev_cwd = Path.cwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


def download(
    url: str,
    file_path: Optional[Union[str, Path]] = Path("."),
    file_name: Optional[str] = None,
    name: str = "Download progress",
    block_size: int = 1024,
) -> Path:

    """Download a file from a url and display a progress bar

    Parameters
    ----------
    url : str
        The url to download the file from
    file_path : str | Path, optional
        The path to save the file to, by default Path(".")
    file_name : str, optional
        The name of the file to save, by default None. If None, the name will be
        extracted from the url.
    name : str, optional
        The name of the progress bar, by default "Download progress"
    block_size : int, optional
        The size of the block to download, by default 1024

    Returns
    -------
    Path
        The path to the downloaded file

    """
    file_path = Path(file_path)
    resp = requests.get(url, stream=True)
    total_size_in_bytes = int(resp.headers.get("content-length", 0))
    tqdm_bar = tqdm(total=total_size_in_bytes, unit="iB", unit_scale=True, desc=name)

    if not file_name:
        file_name = url.split("/")[-1]
    file_path = file_path / file_name

    # Start processing the download
    with open(file_path, "wb") as file:
        for data in resp.iter_content(block_size):
            tqdm_bar.update(len(data))
            file.write(data)
    tqdm_bar.close()

    return file_path


def get_md5sum(filename: Union[str, Path]) -> str:
    """Get the md5sum of a file

    Parameters
    ----------
    filename : str | Path

    Returns
    -------
    str
        The md5sum of the file
    """
    return hashlib.md5(Path(filename).read_bytes()).hexdigest()
