#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import annotations

from pathlib import Path

import plumbum
from filelock import FileLock
from loguru import logger
from pydantic import FilePath, validate_arguments

from ..utils import log_cmd, warn_missing

try:
    _MCFINDER_CMD = plumbum.local["macsyfinder"]
    _MCSYDATA_CMD = plumbum.local["macsydata"]
except plumbum.CommandNotFound:
    logger.warning(warn_missing("macsyfinder cli"))


def download_defensefinder_models(model_dir: Path, *, upgrade=False):
    """ Download the defense-finder models from macsydata.

    Parameters
    ----------
    model_dir : Path
        Models emplacement.
    upgrade : bool, optional
        Whether to upgrade the models if they are already present. Defaults to False.

    """
    cmd = _MCSYDATA_CMD[
        "install", "--org", "mdmparis", "defense-finder-models", "--target", model_dir
    ]
    if upgrade:
        cmd = cmd["--upgrade"]
    logger.info(log_cmd(cmd))
    model_dir.mkdir(exist_ok=True)
    with FileLock(model_dir / ".lock"):
        stderr = cmd.run()[2]
    if stderr:
        logger.info(stderr)
    return


@validate_arguments
def defensefinder(
    island_faa_input: FilePath,
    out_dir: Path,
    *,
    model_dir: Path,
    download_model=False,
    cpu=2,
) -> Path:
    """Run macsyfinder with the defense-finder models on the aminoacid fasta file
    `island_faa_input` and save the results in `out_dir`. Download the defense-finder
    models if needed.

    Parameters
    ----------
    island_faa_input : FilePath
        aminoacid fasta file
    out_dir : Path
        output folder
    model_dir : Optional[Path], optional
        Models emplacement. Use default macsydata folder is set to None. Defaults to None
    download_model : bool
        whether to download or not the defense-finder models. Will not redownload the model
        if it exists. Multi-process safe. Defaults to True.

    Returns
    -------
    Path
        output folder

    Notes
    -----
    The mcfinder command is:

        macsyfinder --db-type ordered_replicon \
                    --models defense-finder-models \
                    --coverage-profile 0.1 \
                    --w <cpu> \
                    --sequence-db <island_faa_input> \
                    --models-dir <model_dir> \
                    --out-dir <out_dir>
    """
    if download_model:
        download_defensefinder_models(model_dir)
    cmd = _MCFINDER_CMD[
        "--db-type",
        "ordered_replicon",
        "--models",
        "defense-finder-models",
        "all",
        "--coverage-profile",
        "0.1",
        "--w",
        cpu,
        "--sequence-db",
        island_faa_input,
        "--models-dir",
        model_dir,
        "--out-dir",
        out_dir,
    ]
    logger.info(log_cmd(cmd))
    logger.debug(cmd())
    return out_dir
