""" This module contains the classes that handle the islands and their annotations.

The Island class is the main class of this module. It contains the sequence of the
island, the annotations, and the sequence handler. The sequence handler is a class
that contains the sequence of the island and the annotations. It is used to get the
sequence of the island, the annotations, and the coordinates of the annotations.

The CDS class is a dataclass that contains the coordinates of the CDS, the strand,
the sequence, and the annotations of the CDS.

The Annotation class is a dataclass that contains the origin of the annotation and
the qualifiers of the annotation.
"""

from __future__ import annotations

import simplejson
from dataclasses import InitVar, asdict, field
from functools import cached_property
from typing import Any, Dict, List, Literal, Optional, Tuple, Union

from entrez_fetcher import GenomeStore
from intervaltree import Interval
from loguru import logger
from pydantic.dataclasses import dataclass
from typing_extensions import Self

from .seq_handler import (
    SeqHandler,
    categorise_cds,
    coords_to_areas,
    get_distance,
    get_island_bounds,
)

Strand = Literal["+", "-"]


@dataclass
class Annotation:
    """Annotation dataclass.

    Parameters
    ----------
    origin : str
        The origin of the annotation.

    quals : dict, optional
        The qualifiers of the annotation, by default, an empty dict.
    """

    origin: str
    quals: dict = field(default_factory=dict)


@dataclass
class CDS:
    """CDS dataclass. A CDS is a coding sequence.

    Parameters
    ----------
    start : int
        The start of the CDS.
    end : int
        The end of the CDS.
    strand : Literal["+", "-"]
        The strand of the CDS.
    area : str
        The area of the CDS.
    bound_distance : Optional[int]
        The distance to the closest bound, by default None.
    annotations : List[Annotation], optional
        The annotations of the CDS, by default an empty list.
    """

    start: int
    end: int
    strand: Strand
    area: str
    bound_distance: Optional[int]
    annotations: List[Annotation] = field(default_factory=list)

    @property
    def interval(self):
        return Interval(self.start, self.end)

    @property
    def size(self):
        return len(self)

    def __len__(self):
        return abs(self.start - self.end)

    def add_annotation(self, origin, quals=None):
        if (annot := Annotation(origin, quals)) in self.annotations:
            logger.warning("Duplicated annotation {annot}.\nSkip and continue")
            return
        self.annotations.append(annot)


@dataclass
class Island:
    """Island dataclass. An island is a sequence of a genome that is bounded by
    specific regions.

    Parameters
    ----------
    accession : str
        The accession of the genome.
    start : int
        The start of the island.
    end : int
        The end of the island.
    host_size : Optional[int], optional
        The size of the host genome, by default None.
    version : Optional[int], optional
        The version of the genome, by default None.
    tax_id : Optional[int], optional
        The taxonomic id of the genome, by default None.
    label : Optional[str], optional
        The label of the island, by default None.
    extra : dict, optional
        Extra information about the island, by default an empty dict.
    CDSs : Dict[Tuple[int, int], CDS], optional
        The CDSs of the island, by default an empty dict. Keys are the start and end
        of the CDS stored in a tuple
    genome_store : InitVar[Optional[GenomeStore]], optional
        The genome store to use, by default None.

    Attributes
    ----------
    bounds : List[Tuple[int, int]]
        The bounds of the island.
    areas : List[Tuple[str, int, int]]
        The areas of the island and their bounds
    seq_handler : SeqHandler
        The sequence handler of the island. It is used to get the sequence of the
        different areas of the island (see SeqHandler for more information).
    """

    accession: str
    start: int
    end: int
    host_size: Optional[int] = None
    version: Optional[int] = None
    tax_id: Optional[int] = None
    label: Optional[str] = None
    extra: dict = field(default_factory=dict, repr=False)
    CDSs: Dict[Tuple[int, int], CDS] = field(default_factory=dict)
    genome_store: InitVar[Optional[GenomeStore]] = None

    def __post_init__(self, genome_store):
        if genome_store is None:
            genome_store = GenomeStore()
        self.genome_store = genome_store

    def __post_init_post_parse__(self, genome_store):
        if self.host_size is None:
            self.host_size = len(self.genome_store.get_genome_record(self.accession))
        self.annotate_cds()

    @property
    def id(self):
        return f"{self.accession}:{self.start}..{self.end}"

    def add_annotation(
        self, start: int, end: int, strand: Strand, origin: str, quals=None
    ):
        """Add an annotation to the island.

        Parameters
        ----------
        start : int
            The start of the annotation.
        end : int
            The end of the annotation.
        strand : Strand
            The strand of the annotation.
        origin : str
            The origin of the annotation.
        quals : dict, optional
            The qualifiers of the annotation, by default None. If None, an empty dict
            is used.
        """
        if (key := (start, end)) not in self.CDSs:
            area = categorise_cds(start, end, self.areas)
            bound_distance = get_distance(start, end, self.bounds, area)
            self.CDSs[key] = CDS(start, end, strand, area, bound_distance)
        self.CDSs[key].add_annotation(origin, quals)

    def annotate(self, annotator: "Annotator"):
        """Annotate the island via an annotator.

        Parameters
        ----------
        annotator : Annotator
            The annotator to use.
        """
        return annotator.annotate(self)

    def add_cds(self, start: int, end: int, strand: Strand) -> None:
        """Add a CDS to the island.

        Parameters
        ----------
        start : int
            The start of the CDS.
        end : int
            The end of the CDS.
        strand : Literal["+", "-"]
            The strand of the CDS.
        """

        if (key := (start, end)) not in self.CDSs:
            area = categorise_cds(start, end, self.areas)
            bound_distance = get_distance(start, end, self.bounds, area)
            self.CDSs[key] = CDS(start, end, strand, area, bound_distance)

    @cached_property
    def bounds(self):
        return get_island_bounds(self.start, self.end, self.host_size)

    @cached_property
    def areas(self):
        return coords_to_areas(self.bounds)

    @cached_property
    def seq_handler(self) -> SeqHandler:
        return SeqHandler(
            areas=self.areas,
            genome_filename=self.genome_store.get_genome_filename(self.accession),
        )

    def annotate_cds(self) -> None:
        """Annotate the CDSs of the island."""
        for cds in self.seq_handler.cds_info:
            self.add_cds(cds.cds_start, cds.cds_end, cds.cds_strand)

    @classmethod
    def from_serie(cls, serie, label=None, ignore_cols=None, **kwargs) -> Self:
        """Create an island from a pandas serie.

        Parameters
        ----------
        serie : pd.Series
            The serie to use.
        label : Optional[str], optional
            The label of the island, by default None.
        ignore_cols : Optional[List[str]], optional
            The columns to ignore, by default None.
        **kwargs
            The extra arguments to pass to the class constructor.

        Returns
        -------
        Island
            The island created from the serie.
        """
        if ignore_cols is not None:
            serie = serie.drop(ignore_cols)
        data = serie.dropna().to_dict()
        if serie.name is not None:
            accession, start, end = serie.name
        else:
            accession = data.pop("accession")
            start = int(data.pop("start"))
            end = int(data.pop("end"))
        try:
            host_size = int(data.pop("host_size"))
        except KeyError:
            host_size = None
        return cls(
            accession=accession,
            start=int(start),
            end=int(end),
            host_size=host_size,
            version=data.pop("version", None),
            label=label,
            extra=data,
            **kwargs,
        )

    def to_dict(self, keep_empty_cds: bool = True) -> Dict[str, Any]:
        """Convert the island to a dict.

        Parameters
        ----------
        keep_empty_cds : bool, optional
            Whether to keep the CDSs with no annotations, by default True.

        Returns
        -------
        Dict[str, Any]
            The dict representation of the island.
        """
        to_serialize = asdict(self)
        to_serialize["coords"] = self.bounds
        to_serialize["CDSs"] = {
            f"{start}:{end}": cds
            for (start, end), cds in sorted(to_serialize["CDSs"].items())
            if (True if keep_empty_cds else bool(cds["annotations"]))
        }
        return to_serialize

    def to_json(
        self,
        filename: Optional[str] = None,
        keep_empty_cds: bool = True,
        *args,
        **kwargs,
    ) -> Union[Dict, str]:
        """Convert the island to a json. If filename is None, the json is returned as a
        string.

        Parameters
        ----------
        filename : Optional[str], optional
            The filename to save the json to, by default None.
        keep_empty_cds : bool, optional
            Whether to keep the CDSs with no annotations, by default True.
        *args
            The extra arguments to pass to json.dump.
        **kwargs
            The extra arguments to pass to json.dump.
        """
        to_serialize = self.to_dict(keep_empty_cds=keep_empty_cds)
        if filename is None:
            return simplejson.dumps(
                to_serialize,
                default=lambda obj: getattr(obj, "tolist", lambda: obj)(),
                ignore_nan=True,
                *args,
                **kwargs,
            )
        with open(filename, "w") as f:
            simplejson.dump(
                to_serialize,
                f,
                default=lambda obj: getattr(obj, "tolist"),
                ignore_nan=True,
                *args,
                **kwargs,
            )

    @classmethod
    def from_json(cls, filename: str) -> Self:
        """Create an island from a json file.

        Parameters
        ----------
        filename : str
            The filename of the json file.

        Returns
        -------
        Island
            The island created from the json file.
        """
        with open(filename) as f:
            to_load: dict = simplejson.load(f)
        return cls.from_dict(to_load)

    @classmethod
    def from_dict(cls, to_load: Dict[str, Any]) -> Self:
        """ Create an island from a dict.

        Parameters
        ----------
        to_load : Dict[str, Any]
            The dict to use.

        Returns
        -------
        Island
            The island created from the dict.
        """
        try:
            CDSs = to_load.pop("CDSs")
        except KeyError:
            CDSs = {}
        to_load.pop("coords")
        island = cls(**to_load)
        for CDS in CDSs.values():
            for annotation in CDS["annotations"]:
                island.add_annotation(
                    CDS["start"],
                    CDS["end"],
                    CDS["strand"],
                    annotation["origin"],
                    annotation["quals"],
                )
        return island
