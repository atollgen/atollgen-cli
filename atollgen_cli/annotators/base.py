from __future__ import annotations

from abc import ABCMeta, abstractmethod
from typing import Literal

from pandas import DataFrame

from ..islands import Island
from ..seq_handler import Area


def add_annot_from_df(island: Island, df: DataFrame, origin: str) -> Island:
    """Add annotations from a dataframe. Be careful, the island will be modified in
    place.

    Parameters
    ----------
    island : Island
        The island to annotate.
    df : DataFrame
        The dataframe containing the annotations. The columns must be the same
        as the quals of the annotation.
    origin : str
        The origin of the annotation.

    Returns
    -------
    Island
        The annotated island.
    """

    if not (cds_df := DataFrame(island.seq_handler.cds_info)).empty:
        df = df.join(
            cds_df.set_index("cds_id")[["cds_start", "cds_end", "cds_strand"]],
            on="hit_id",
        )
    for _, row in df.iterrows():
        row = row.to_dict()
        island.add_annotation(
            start=row.pop("cds_start"),
            end=row.pop("cds_end"),
            strand=row.pop("cds_strand"),
            origin=origin,
            quals=row,
        )
    return island


class Annotator(metaclass=ABCMeta):
    """Base class for annotators. An annotator is a class that will annotate an island
    with a specific tool.

    Parameters
    ----------
    on : Literal["faa", "fna"]
        The kind of sequence to annotate.
    area : Area
        The area of the island to annotate.
    """

    def __init__(self, on: Literal["faa", "fna"], area: Area) -> None:
        self.on = on
        self.area = area

    def path(self, island: Island):
        """Return the path where to find the sequence on which the annotator will work.

        Parameters
        ----------
        island : Island
            The island to annotate.

        Returns
        -------
        Path
            The path to the sequence.
        """
        return island.seq_handler.path(kind=self.on, area=self.area)

    @abstractmethod
    def annotate(self, island: Island) -> NotImplemented:
        raise NotImplementedError

    def check(self):
        """Check if the annotator can be used. If not, raise an exception."""
        return
