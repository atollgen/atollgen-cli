""" Annotators for Atollgen

This module contains the classes for the different annotators used by Atollgen.
"""

from.base import Annotator
from .defense_finder import DFinderAnnotator
from .hmm import HmmerAnnotator
from .rgi import RGIAnnotator
from .trnascan import TrnaScanAnnotator

__all__ = ["DFinderAnnotator", "HmmerAnnotator", "RGIAnnotator", "TrnaScanAnnotator"]